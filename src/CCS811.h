/*
CCS811.h - CCS811 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

// Note: registers related to "boot mode" are not implemented
// I have no interest in writing new firmware to a sensor!

class CCS811 {
private:
  enum class Register : uint8_t {
    STATUS		= 0x00,	// 1 byte, R/O
    MEAS_MODE,			// 1 byte, R/W
    ALG_RESULT_DATA,		// up to 8 bytes, R/O
    RAW_DATA,			// 2 bytes, R/O

    ENV_DATA		= 0x05,	// 4 bytes, W/O

    THRESHOLDS		= 0x10,	// 4 bytes, W/O
    BASELINE,			// 2 bytes, R/W

    HW_ID		= 0x20,	// 1 byte, R/O
    HW_VERSION,			// 1 byte, R/O

    FW_BOOT_VERSION	= 0x23,	// 2 bytes, R/O
    FW_APP_VERSION,		// 2 bytes, R/O

    INTERNAL_STATE	= 0xa0,	// 1 byte, R/O

    ERROR_ID		= 0xe0,	// 1 byte, R/O
  };

  enum class Status_mask : uint8_t {
    ERROR		= 0x01,

    DATA_READY		= 0x08,
    APP_VALID		= 0x10,
    FW_MODE		= 0x80,
  };

  enum class Measure_mode_mask : uint8_t {
    INT_THRESH		= 0x04,
    INT_DATARDY		= 0x08,
    DRIVE_MODE		= 0x70,
  };

  enum class Drive_mode : uint8_t {
    Off			= 0x00,	// Idle
    Every_second	= 0x10,	// Constant power mode
    Every_10seconds	= 0x20,	// Pulse-heating mode
    Every_60seconds	= 0x30,	// Low power pulse-heating mode
    Every_250millis	= 0x40,	// Constant power mode
    // other values are reserved
  };

  enum class Error_id_mask : uint8_t {
    WRITE_REG_INVALID	= 0x01,
    READ_REG_INVALID	= 0x02,
    MEASMODE_INVALID	= 0x04,
    MAX_RESISTANCE	= 0x08,
    HEATER_FAULT	= 0x10,
    HEATER_SUPPLY	= 0x20,
    // bits 6 and 7 reserved for future use
  };

  uint8_t _addr;

  bool _read_reg(Register reg, uint8_t* data, uint8_t len);
  bool _write_reg(Register reg, uint8_t* data, uint8_t len);

  // _alg_result_data contains status at index 4, error_id at index 5, raw data at indexes 6&7
  uint8_t _meas_mode, _alg_result_data[8];

public:
  CCS811(uint8_t addr = 0x5a);

  bool read_status(void);
  bool error(void)		{ return _alg_result_data[4] & static_cast<uint8_t>(Status_mask::ERROR); }
  bool data_ready(void)		{ return _alg_result_data[4] & static_cast<uint8_t>(Status_mask::DATA_READY); }
  bool app_valid(void)		{ return _alg_result_data[4] & static_cast<uint8_t>(Status_mask::APP_VALID); }
  bool fw_mode(void)		{ return _alg_result_data[4] & static_cast<uint8_t>(Status_mask::FW_MODE); }

  bool read_meas_mode(void);
  bool int_threashold(void)	{ return _meas_mode & static_cast<uint8_t>(Measure_mode_mask::INT_THRESH); }
  void set_int_threashold(bool val = true);
  bool int_dataready(void)	{ return _meas_mode & static_cast<uint8_t>(Measure_mode_mask::INT_DATARDY); }
  void set_int_dataready(bool val = true);
  Drive_mode drive_mode(void)	{ return static_cast<Drive_mode>(_meas_mode & static_cast<uint8_t>(Measure_mode_mask::DRIVE_MODE)); }
  void set_drive_mode(Drive_mode m);
  bool write_meas_mode(void);

  bool read_algorithm_result(void);
  uint16_t eCO2(void)		{ return ((uint16_t)_alg_result_data[0] << 8) | _alg_result_data[1]; }
  uint16_t eTVOC(void)		{ return ((uint16_t)_alg_result_data[2] << 8) | _alg_result_data[3]; }

  bool read_raw_data(void);
  uint8_t raw_current(void)	{ return _alg_result_data[6] >> 2; }
  uint16_t raw_voltage(void)	{ return (((uint16_t)_alg_result_data[6] & 0x03) << 8) | _alg_result_data[7]; }

  bool write_env_data(float humidity, float temperature);
  bool write_thresholds(uint16_t low, uint16_t high);

  bool read_baseline(uint8_t* data);
  bool write_baseline(uint8_t* data);

  uint8_t hw_id(void);
  uint8_t hw_version(void);
  uint16_t fw_boot_version(void);
  uint16_t fw_app_version(void);

  bool read_error_id(void);
  bool write_reg_invalid(void)	{ return _alg_result_data[5] & static_cast<uint8_t>(Error_id_mask::WRITE_REG_INVALID); }
  bool read_reg_invalid(void)	{ return _alg_result_data[5] & static_cast<uint8_t>(Error_id_mask::READ_REG_INVALID); }
  bool measmode_invalid(void)	{ return _alg_result_data[5] & static_cast<uint8_t>(Error_id_mask::MEASMODE_INVALID); }
  bool max_resistance(void)	{ return _alg_result_data[5] & static_cast<uint8_t>(Error_id_mask::MAX_RESISTANCE); }
  bool heater_fault(void)	{ return _alg_result_data[5] & static_cast<uint8_t>(Error_id_mask::HEATER_FAULT); }
  bool heater_supply(void)	{ return _alg_result_data[5] & static_cast<uint8_t>(Error_id_mask::HEATER_SUPPLY); }

}; // class CCS811
