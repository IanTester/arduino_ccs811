/*
CCS811.cpp - CCS811 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <Arduino.h>
#include <Wire.h>
#include "CCS811.h"

CCS811::CCS811(uint8_t addr) :
  _addr(addr),
  _meas_mode(0),
  _alg_result_data{0, 0, 0, 0, 0, 0, 0, 0}
{
}

bool CCS811::_read_reg(Register reg, uint8_t* data, uint8_t len) {
  Wire.beginTransmission(_addr);
  size_t written = Wire.write(static_cast<uint8_t>(reg));
  Wire.endTransmission();
  if (written == 0) {
    return false;
  }

  delay(10);

  size_t read = Wire.requestFrom(_addr, len);
  if (read < len) {
    return false;
  }

  for (uint8_t i = 0; i < 8; i++) {
    int d = Wire.read();
    if (d == -1) {
      return false;
    }
    data[i] = d;
  }

  Wire.endTransmission();

  return true;
}

bool CCS811::_write_reg(Register reg, uint8_t* data, uint8_t len) {
  Wire.beginTransmission(_addr);

  size_t written = Wire.write(static_cast<uint8_t>(reg));
  if (written == 0) {
    return false;
  }

  written = Wire.write(data, len);
  if (written < len) {
    return false;
  }

  Wire.endTransmission();

  return true;
}

bool CCS811::read_status(void) {
  return _read_reg(Register::STATUS, _alg_result_data + 4, 1);
}

bool CCS811::read_meas_mode(void) {
  return _read_reg(Register::MEAS_MODE, &_meas_mode, 1);
}

void CCS811::set_int_threashold(bool val) {
  if (val)
    _meas_mode |= static_cast<uint8_t>(Measure_mode_mask::INT_THRESH);
  else
    _meas_mode &= ~static_cast<uint8_t>(Measure_mode_mask::INT_THRESH);
}

void CCS811::set_int_dataready(bool val) {
  if (val)
    _meas_mode |= static_cast<uint8_t>(Measure_mode_mask::INT_DATARDY);
  else
    _meas_mode &= ~static_cast<uint8_t>(Measure_mode_mask::INT_DATARDY);
}

void CCS811::set_drive_mode(Drive_mode m) {
  _meas_mode &= ~static_cast<uint8_t>(Measure_mode_mask::DRIVE_MODE);
  _meas_mode |= static_cast<uint8_t>(m);
}

bool CCS811::write_meas_mode(void) {
  return _write_reg(Register::MEAS_MODE, &_meas_mode, 1);
}

bool CCS811::read_algorithm_result(void) {
  return _read_reg(Register::ALG_RESULT_DATA, _alg_result_data, 8);
}

bool CCS811::read_raw_data(void) {
  return _read_reg(Register::RAW_DATA, _alg_result_data + 6, 2);
}

bool CCS811::write_env_data(float humidity, float temperature) {
  uint16_t hum_val;
  if (humidity < 0)
    hum_val = 0;
  else if (humidity > 127.9980469)
    hum_val = 65535;
  else
    hum_val = humidity * 512;

  uint16_t temp_val;
  if (temperature < -25)
    temp_val = 0;
  else if (temperature > 102.9980469)
    temp_val = 65535;
  else
    temp_val = (temperature + 25) * 512;

  uint8_t data[4] = { hum_val >> 8, hum_val & 0xff, temp_val >> 8, temp_val & 0xff };
  return _write_reg(Register::ENV_DATA, data, 4);
}

bool CCS811::write_thresholds(uint16_t low, uint16_t high) {
  uint8_t data[4] = { low >> 8, low & 0xff, high >> 8, high & 0xff };
  return _write_reg(Register::THRESHOLDS, data, 4);
}

bool CCS811::read_baseline(uint8_t* data) {
  return _read_reg(Register::BASELINE, data, 2);
}

bool CCS811::write_baseline(uint8_t* data) {
  return _write_reg(Register::BASELINE, data, 2);
}

uint8_t CCS811::hw_id(void) {
  uint8_t val;
  _read_reg(Register::HW_ID, &val, 1);
  return val;
}

uint8_t CCS811::hw_version(void) {
  uint8_t val;
  _read_reg(Register::HW_VERSION, &val, 1);
  return val;
}

uint16_t CCS811::fw_boot_version(void) {
  uint16_t val;
  _read_reg(Register::FW_BOOT_VERSION, (uint8_t*)&val, 2);
  return val;
}

uint16_t CCS811::fw_app_version(void) {
  uint16_t val;
  _read_reg(Register::FW_APP_VERSION, (uint8_t*)&val, 2);
  return val;
}

bool CCS811::read_error_id(void) {
  return _read_reg(Register::ALG_RESULT_DATA, _alg_result_data + 5, 1);
}
